// load the things we need
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var mongoose = require('mongoose');
var mongoDB = 'mongodb://Paolo:123456789@ds163705.mlab.com:63705/server2project';
var passport = require('passport');
var morgan = require('morgan');
var session = require('express-session');
var flash =require('connect-flash');

var app = express();
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());

require('./config/passport')(passport);

//passport
app.use(session({secret:'couldwemeetagainwhenboutourcarscollide'}))
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

mongoose.connect(mongoDB, {
  useMongoClient: true
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//usar las rutas
var routerHome = require('./routes/routerHome');
var users = require('./routes/users')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator()); // Add this after the bodyParser middlewares!
app.use(express.static(path.join(__dirname, 'public')))

app.use('/',routerHome);
app.use('/user',users)

//manejo errores
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

// use res.render to load up an ejs view file

// about page 


app.listen(9090);
console.log('9090 is the magic port');