var Blog = require('../models/m_blog');

//ddddd

// Display list of all Blogs
exports.Blog_list = function(req, res,next) {
    Blog.find({})
    .exec(function(err,list_blog){
        if(err){return next(err);}
        res.render('blog',{
            title: 'lista',
            blogs_list: list_blog }); 
    }
    );
    
};

// Display detail page for a specific Blog
exports.Blog_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Blog detail: ' + req.params.id);
};

// Display Blog create form on GET
exports.Blog_create_get = function(req, res) {
   res.render('crearBlog',{title:'Crear blog'})
};

// Handle Blog create on POST
exports.Blog_create_post = function(req, res,next) {
    req.checkBody('nombre','Autor requerido').notEmpty();
//    req.checkBody('fecha','Escoger Fecha').notEmpty();
    req.checkBody('contenido','Escoger Fecha').notEmpty();
    
    req.sanitize('nombre').escape();
    req.sanitize('nombre').trim();

    // req.sanitize('fecha').escape();
    // req.sanitize('fecha').trim();
    
    req.sanitize('contenido').escape();
    req.sanitize('contenido').trim();

   var errors = req.validationErrors();
    
    var blog = new Blog(
        {
            Autor: req.body.nombre,
            Hora: "00:00",
            Fecha: "12/12/17",
            Contenido: req.body.contenido
            
        }
    )

    if(errors)
    {
        res.render('crearBlog')
        return;
    }else{
        blog.save(function(err)
    {
        if(err){return next(err)}
        res.redirect('blog');
    }    
    )
    }

};

// Display Blog delete form on GET
exports.Blog_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Blog delete GET');
};

// Handle Blog delete on POST
exports.Blog_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Blog delete POST');
};

// Display Blog update form on GET
exports.Blog_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Blog update GET');
};

// Handle Blog update on POST
exports.Blog_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Blog update POST');
};