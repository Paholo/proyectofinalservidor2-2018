var Foro = require('../models/m_foro');

// Display list of all Foros
exports.Foro_list = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro list');
};

// Display detail page for a specific Foro
exports.Foro_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro detail: ' + req.params.id);
};

// Display Foro create form on GET
exports.Foro_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro create GET');
};

// Handle Foro create on POST
exports.Foro_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro create POST');
};

// Display Foro delete form on GET
exports.Foro_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro delete GET');
};

// Handle Foro delete on POST
exports.Foro_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro delete POST');
};

// Display Foro update form on GET
exports.Foro_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro update GET');
};

// Handle Foro update on POST
exports.Foro_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Foro update POST');
};