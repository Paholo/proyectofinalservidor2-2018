var express = require('express');
var router = express.Router();
var passport = require('passport');
var blog_controller = require('../controllers/controllerBlog');

/* GET users listing. */

router.get('/foro', function(req, res) {
    res.render('foro');
});

router.get('/crearBlog', blog_controller.Blog_create_get);
router.post('/crearBlog', blog_controller.Blog_create_post);

router.get('/about', function(req, res) {
    res.render('about');
});

router.get('/blog', blog_controller.Blog_list);
// function(req, res) {
//     res.render('blog');
// });

router.get('/sesions', function(req, res) {
    res.render('sesions');
});

router.get('/data', function(req, res) {
    res.render('data');
});

router.get('/', function(req, res){
    res.render('index',{users : [
        { name: 'Pato' },
        { name: 'Paolo' },
        { name: 'Kevin' },
        { name: 'Gabriel' }
    ]});
});

router.get('/login',function(req,res){
    res.render('login', {message: req.flash('loginMessage')});
})

router.post('/login',passport.authenticate('local-login',{
    successRedirect:  '/profile',
    failureRedirect: '/login',
    failureFlash:true
}));

router.get('/signup',function(req,res){
    res.render('signup',{message: req.flash('signupMessage')});
})

router.post('/signup',passport.authenticate('local-signup',{
    successRedirect: '/profile',
    failureRedirect:'/signup',
    failureFlash:true
}));

router.get('/profile',isLoggedIn,function(req,res){
    res.render('profile',{user: req.user});
})

router.get('/logout',function(req,res){
    req.logout();
    res.redirect('/');
})

function isLoggedIn(req,res,next){
    if(req.isAuthenticated())
        return next();

        res.redirec('/');
}

module.exports = router;