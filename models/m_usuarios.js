var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Schema = mongoose.Schema;

var UserSchema = new Schema(
  {
    Nombre: {type: String, /*required: true,*/ max: 100},
    Apellido: {type: String, /*required: true,*/ max: 100},
    Mail: {type: String, required:true},
    Password: {type: String, require:true},
  }
);

// Virtual for author's full name
UserSchema
.virtual('nombre')
.get(function () {
  return this.Nombre + ', ' + this.Apellido;
});

UserSchema.methods.generateHash = function(password){
  return bcrypt.hashSync(password,bcrypt.genSaltSync(8),null);
};

UserSchema.methods.validPassword = function(password){
  return bcryptcompareSync(password, this.local.password);
};

//Export model
module.exports = mongoose.model('User', UserSchema);