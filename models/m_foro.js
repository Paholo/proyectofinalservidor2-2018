var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ForoSchema = new Schema(
  {
    Autor: {type: String, required: true, max: 100},
    Fecha: {type: String, required: true}, // DD/MM/YY
    Hora: {type: String, required:true}, // 24:00
    Contenido: {type: String, require:true},
    numb_Discusiones: {type:Number},
    Discucion: [
        {
            ID:{type: Number},
            Autor:{type:String},
            Fecha:{type:String},
            Hora:{type:String},
            Contenido:{type:String}
        }
    ]
  }
);

// Virtual para autor con fecha y hora 
ForoSchema
.virtual('AutorFecha')
.get(function () {
  return this.Autor + ', ' + this.Fecha+ ',' + this.Hora;
});

//Export model
module.exports = mongoose.model('Foro', ForoSchema);