var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BlogSchema = new Schema(
  {
    Autor: {type: String, required: true, max: 100},
    Fecha: {type: String, required: true}, // DD/MM/YY
    Hora: {type: String, required:true}, // 24:00
    Contenido: {type: String, require:true},
  }
);

// Virtual para autor con fecha y hora 
BlogSchema
.virtual('AutorFecha')
.get(function () {
  return this.Autor + ', ' + this.Fecha+ ',' + this.Hora;
});

//Export model
module.exports = mongoose.model('Blog', BlogSchema);