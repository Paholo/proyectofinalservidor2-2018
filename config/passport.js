var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/m_usuarios');

module.exports = function(passport){
    passport.serializeUser(function(user,done){
        done(null, user.id)
    });

    passport.deserializeUser(function(id,done){
        User.findById(id, function(err,user){
            done(err,user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        usernameField: 'Mail',
        passwordField: 'Password',
        passReqToCallback: true
    },
    function(req,email,password,done)
    {
        process.nextTick(function(){
            User.findOne({'local.Mail': email}, function(err, user){
                if(err)
                    return done(err);

                if(user){
                    return done(null,false,req.flash('signupMessage','Ese email ya ha sido utilizado '));
                }else{
                    var newUser = new User();
                    newUser.local.Mail = email;
                    newUser.local.Password = newUser.generateHash(password);

                    newUser.save(function(err){
                        if(err)
                        {
                            throw err;
                            
                        }
                        return done(null,newUser);
                    });
                }
            });
        });
    }
    ));


    //login
    passport.use('local-login', new LocalStrategy({
        usernameField:'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req,email,password,done){
        User.findOne({'local.Mail':email},function(err,user){
            if(err)
                return done(err);

            if(!user)
                return done(null,false,req.flash('loginMessage','usuario no existe'));    
            
                
            if(!user.validPassword(password))
                return done(null,false,req.flash('loginMessage','contrasena icorrecta'))    
        
            return done(null, user);

            });
    }
    ));
    
};




